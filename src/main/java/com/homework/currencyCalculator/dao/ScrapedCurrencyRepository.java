package com.homework.currencyCalculator.dao;

import java.util.List; 

import org.springframework.data.jpa.repository.JpaRepository;

import com.homework.currencyCalculator.entity.ScrapedCurrency;

public interface ScrapedCurrencyRepository extends JpaRepository<ScrapedCurrency, Integer> {
	
	public List<ScrapedCurrency> findAllByOrderByIdAsc();

}
