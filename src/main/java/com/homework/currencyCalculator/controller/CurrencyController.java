package com.homework.currencyCalculator.controller;

import java.math.BigDecimal;  
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.homework.currencyCalculator.entity.CombinedCurrencies;
import com.homework.currencyCalculator.entity.Currency;
import com.homework.currencyCalculator.entity.ScrapedCurrency;
import com.homework.currencyCalculator.service.CombinedCurrencyService;
import com.homework.currencyCalculator.service.CurrencyExchange;
import com.homework.currencyCalculator.service.CurrencyService;
import com.homework.currencyCalculator.service.ScraperService;

@Controller
@RequestMapping("/currencies")
public class CurrencyController {

	private CurrencyService currencyService;
	private CurrencyExchange currencyExchange;
	private ScraperService scraperService;
	private CombinedCurrencyService combinedCurrencyService;
	
	public CurrencyController(CurrencyService theCurrencyService, CurrencyExchange theCurrencyExchange, 
			ScraperService thescraScraperService, CombinedCurrencyService theCombinedCurrencyService) {
		currencyService = theCurrencyService;
		currencyExchange = theCurrencyExchange;
		scraperService = thescraScraperService;
		combinedCurrencyService = theCombinedCurrencyService;
	}
	
	@GetMapping("/list")
	public String listCurrencies(Model theModel) {
		List<CombinedCurrencies> theCurrencies = combinedCurrencyService.findAll();
		
		theModel.addAttribute("currencies", theCurrencies);
		
		return "currencies/list-currencies";
	}
	
	@GetMapping("/edit")
	public String editCurrencies(Model theModel) {
		List<CombinedCurrencies> theCurrencies = combinedCurrencyService.findAll();
		
		theModel.addAttribute("currencies", theCurrencies);
		
		return "admin/edit-currencies";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {
		
		Currency theCurrency = new Currency();
		
		theModel.addAttribute("currencies", theCurrency);
		
		return "admin/currency-form";
	}
	
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("currencyId") int theId, Model theModel) {
		
		CombinedCurrencies theCurrency = combinedCurrencyService.findById(theId);
		
		theModel.addAttribute("currencies", theCurrency);
		
		return "admin/currency-form";
	}
	
	@PostMapping("/save")
	public String saveCurrency(@Valid @ModelAttribute("currencies") CombinedCurrencies theCurrency, BindingResult theBindingResult, Model theModel) {
		
		theModel.addAttribute("currencies", theCurrency);
		theCurrency.setDateTime(LocalDateTime.now(ZoneOffset.ofHours(+6)));
		
		String currencyCode = theCurrency.getCurrencyCode();
		String currencyName = theCurrency.getCurrencyName();
		BigDecimal currencyValue = theCurrency.getUnitValue();
		BigDecimal currencyPrice = theCurrency.getUnitPrice();
		BigDecimal currencySellPrice = theCurrency.getUnitSellPrice();
		
		if (theBindingResult.hasErrors()) {
			return "admin/currency-form";
		} else {
			
			if (scraperService.existingScrapedCurrency(currencyCode) == -1) {
				Currency currency = new Currency();
				currency.setCurrencyCode(currencyCode);
				currency.setUnitValue(currencyValue);
				currency.setUnitPrice(currencyPrice);
				currency.setUnitSellPrice(currencySellPrice);
				currency.setCurrencyName(currencyName);
				currency.setDateTime(LocalDateTime.now(ZoneOffset.ofHours(+6)));
				currencyService.save(currency);
			}
			
			combinedCurrencyService.save(theCurrency);
			return "redirect:/success";
		}
	}
	
	@GetMapping("/delete")
	public String deleteCurrency(@RequestParam("currencyId") int theId) {
		
		String code = combinedCurrencyService.findById(theId).getCurrencyCode();
		
		if (existingCurrency(code) != -1) {
			currencyService.deleteById(existingCurrency(code));
		}
		
		combinedCurrencyService.deleteById(theId);
		
		return "redirect:/success";
	}
	
	@GetMapping("/updateCurrencies")
	public String updateCurrencies(Model theModel) {
		
			List<Currency> theCurrencies = currencyService.findAll();
		
			theModel.addAttribute("currencies", theCurrencies);
			
			List<ScrapedCurrency> scrapedCurrencies = scraperService.getCurrencies();

			scraperService.chechAvailableCombinedCurrencies(scrapedCurrencies);
			currencyService.checkAvailableCurrencies(theCurrencies);
			
			return "redirect:/success";
	}
	
	@RequestMapping("exchangeResult")
	public String exchangeResult(@RequestParam(value =  "currencyAmmount", required = false) BigDecimal ammount, 
			@RequestParam("currencyFrom") int fromCyrrency, @RequestParam("currencyTo") int currencyTo, Model theModel) {
			
			List<CombinedCurrencies> theCurrencies = combinedCurrencyService.findAll();
			theModel.addAttribute("savedCurrencyFrom", fromCyrrency);
			theModel.addAttribute("savedCurrencyTo", currencyTo);
			
			theModel.addAttribute("currencies", theCurrencies);
			
			if(ammount == null) {
				theModel.addAttribute("emptyError", "Input can not be empty!");
				return "currencies/list-currencies";
			}
			
			if(ammount.compareTo(BigDecimal.ZERO) <= 0) {
				theModel.addAttribute("emptyError", "Input can not be zero or a negative number!");
				theModel.addAttribute("currencyAmmount", ammount);
				return "currencies/list-currencies";
			}
			
			if (fromCyrrency == 0 || currencyTo == 0) {
				theModel.addAttribute("emptyCurrency", "Please choose a currency!");
				return "currencies/list-currencies";
			}
			
			CombinedCurrencies currencyFrom = combinedCurrencyService.findById(fromCyrrency);
			CombinedCurrencies toCurrency = combinedCurrencyService.findById(currencyTo);
			if (currencyFrom.getUnitPrice().compareTo(BigDecimal.ZERO) <= 0) {
				theModel.addAttribute("emptyCurrency", "Sorry, You can't use the currency " + currencyFrom.getCurrencyName() + " right now...");
				return "currencies/list-currencies";
			}
			
			if (toCurrency.getUnitPrice().compareTo(BigDecimal.ZERO) <= 0) {
				theModel.addAttribute("emptyCurrency", "Sorry, You can't use the currency " + toCurrency.getCurrencyName() + " right now...");
				return "currencies/list-currencies";
			}
			
			BigDecimal result = currencyExchange.currencyExchange(ammount, fromCyrrency, currencyTo);
			
			theModel.addAttribute("result", result);
			theModel.addAttribute("currencyAmmount", ammount);
			
			theModel.addAttribute("currencyTypeFrom", currencyExchange.currencyAmmount(fromCyrrency, ammount));
			theModel.addAttribute("currencyTypeTo", currencyExchange.currencyResult(currencyTo, result));
		
		return "currencies/list-currencies";
	}
	
	private int existingCurrency(String key) {
		List<Currency> currencyList = currencyService.findAll();
		
		for (Currency currencies : currencyList) {
			if (key.equals(currencies.getCurrencyCode())) {
				return currencies.getId();
			}
		}
		return -1;
	}
}