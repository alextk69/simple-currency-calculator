package com.homework.currencyCalculator.service;

import java.util.List;

import com.homework.currencyCalculator.entity.Currency;

public interface CurrencyService {
	
	public List<Currency> findAll();
	
	public Currency findById(int theId);
	
	public void save(Currency theCurrency);
	
	public void deleteById(int theId);
	
	public void checkAvailableCurrencies(List<Currency> theCurrency);

}
