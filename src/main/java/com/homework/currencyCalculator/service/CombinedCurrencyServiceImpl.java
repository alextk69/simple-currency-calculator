package com.homework.currencyCalculator.service;

import java.util.List; 
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homework.currencyCalculator.dao.CombinedCurrenciesRepository;
import com.homework.currencyCalculator.entity.CombinedCurrencies;

@Service
public class CombinedCurrencyServiceImpl implements CombinedCurrencyService {
	
	private CombinedCurrenciesRepository combinedCurrenciesRepository;
	
	@Autowired
	public CombinedCurrencyServiceImpl(CombinedCurrenciesRepository theCombinedCurrenciesRepository) {
		combinedCurrenciesRepository = theCombinedCurrenciesRepository;
	}

	@Override
	public List<CombinedCurrencies> findAll() {
		
		return combinedCurrenciesRepository.findAllByOrderByIdAsc();
	}

	@Override
	public CombinedCurrencies findById(int theId) {

		Optional<CombinedCurrencies> result = combinedCurrenciesRepository.findById(theId);
		
		CombinedCurrencies theCurrency = null;
		
		if (result.isPresent()) {
			theCurrency = result.get();
		} else {
			throw new RuntimeException("Didn't find currency Id: " + theId);
		}
		
		return theCurrency;
	}

	@Override
	public void save(CombinedCurrencies theCombinedCurrencies) {
		combinedCurrenciesRepository.save(theCombinedCurrencies);
	}

	@Override
	public void deleteById(int theId) {
		combinedCurrenciesRepository.deleteById(theId);
	}

}
