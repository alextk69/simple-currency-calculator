package com.homework.currencyCalculator.service;

import java.util.List;

import com.homework.currencyCalculator.entity.CombinedCurrencies;

public interface CombinedCurrencyService {
	
	public List<CombinedCurrencies> findAll();
	
	public CombinedCurrencies findById(int theId);
	
	public void save(CombinedCurrencies thecoCombinedCurrencies);
	
	public void deleteById(int theId);

}
