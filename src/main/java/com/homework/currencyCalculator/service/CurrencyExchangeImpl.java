package com.homework.currencyCalculator.service;

import java.math.BigDecimal;  
import java.math.MathContext;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homework.currencyCalculator.entity.CombinedCurrencies;

@Service
public class CurrencyExchangeImpl implements CurrencyExchange{
	
	private CombinedCurrencyService combinedCurrencyService;
	
	@Autowired
	public CurrencyExchangeImpl(CombinedCurrencyService theCombinedCurrencyService) {
		combinedCurrencyService = theCombinedCurrencyService;
	}

	@Override
	public BigDecimal currencyExchange(BigDecimal ammount, int fromCurrency, int currencyTo) {
		
		CombinedCurrencies currencyFrom = combinedCurrencyService.findById(fromCurrency);
		CombinedCurrencies toCurrency = combinedCurrencyService.findById(currencyTo);
		
		MathContext mc = new MathContext(6, RoundingMode.HALF_UP);
		
		BigDecimal result = ((ammount.divide(currencyFrom.getUnitValue(), mc)).multiply(currencyFrom.getUnitPrice())).divide(toCurrency.getUnitPrice().divide(toCurrency.getUnitValue(), mc), mc);
		
		return result;
	}

	@Override
	public String currencyResult(int currencyTo, BigDecimal result) {

		CombinedCurrencies toCurrency = combinedCurrencyService.findById(currencyTo);
		
		String theCurrency = toCurrency.getCurrencyName();
		
		if (result.floatValue() != 1.0f) {
			theCurrency += "s";
		}
		
		return theCurrency;
	}

	@Override
	public String currencyAmmount(int fromCurrency, BigDecimal ammount) {

		CombinedCurrencies currencyFrom = combinedCurrencyService.findById(fromCurrency);
		
		String currencyAmmount = currencyFrom.getCurrencyName();
		
		if (ammount.floatValue() != 1.0f) {
			currencyAmmount += "s";
		}
		
		return currencyAmmount;
	}

}
