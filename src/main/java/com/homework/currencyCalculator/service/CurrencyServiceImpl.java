package com.homework.currencyCalculator.service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homework.currencyCalculator.dao.CurrencyRepository;
import com.homework.currencyCalculator.entity.CombinedCurrencies;
import com.homework.currencyCalculator.entity.Currency;

@Service
public class CurrencyServiceImpl implements CurrencyService {
	
	private CurrencyRepository currencyRepository;
	private CombinedCurrencyService combinedCurrencyService;
	private ScraperService scraperService; 
	
	@Autowired
	public CurrencyServiceImpl(CurrencyRepository theCurrencyRepository, CombinedCurrencyService theCombinedCurrencyService, ScraperService theScraperService) {
		currencyRepository = theCurrencyRepository;
		combinedCurrencyService = theCombinedCurrencyService;
		scraperService = theScraperService;
	}

	@Override
	public List<Currency> findAll() {
		
		return currencyRepository.findAllByOrderByIdAsc();
	}

	@Override
	public Currency findById(int theId) {

		Optional<Currency> result = currencyRepository.findById(theId);
		
		Currency theCurrency = null;
		
		if (result.isPresent()) {
			theCurrency = result.get();
		} else {
			throw new RuntimeException("Didn't find currency Id: " + theId);
		}
		
		return theCurrency;
	}

	@Override
	public void save(Currency theCurrency) {
		currencyRepository.save(theCurrency);
	}

	@Override
	public void deleteById(int theId) {
		currencyRepository.deleteById(theId);
	}

	@Override
	public void checkAvailableCurrencies(List<Currency> theCurrency) {
		theCurrency.sort(Comparator.comparing(Currency::getCurrencyCode).thenComparing(Currency::getDateTime));
		Map<String, Currency> currencyMap = theCurrency.stream()
				.collect(Collectors.toMap(Currency::getCurrencyCode, data -> data, (data1, data2) -> data2));
		for (Map.Entry<String, Currency> entry : currencyMap.entrySet()) {
			String key = entry.getKey();
			Currency val = entry.getValue();
			if (scraperService.existingScrapedCurrency(key) != -1) {
				int tempId = scraperService.existingScrapedCurrency(key);
				CombinedCurrencies tempCurrency = combinedCurrencyService.findById(tempId);
				tempCurrency.setUnitValue(val.getUnitValue());
				tempCurrency.setUnitPrice(val.getUnitPrice());
				tempCurrency.setUnitSellPrice(val.getUnitSellPrice());
				tempCurrency.setDateTime(val.getDateTime());
				combinedCurrencyService.save(tempCurrency);
			} else {
				CombinedCurrencies combined = new CombinedCurrencies();
				combined.setCurrencyName(val.getCurrencyName());
				combined.setCurrencyCode(val.getCurrencyCode());
				combined.setUnitValue(val.getUnitValue());
				combined.setUnitPrice(val.getUnitPrice());
				combined.setUnitSellPrice(val.getUnitSellPrice());
				combined.setDateTime(val.getDateTime());
				combinedCurrencyService.save(combined);
			}
			
		}
	}

}
